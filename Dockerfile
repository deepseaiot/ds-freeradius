FROM frauhottelmann/daloradius-docker:1.1
#COPY raddb/ /etc/raddb/
COPY freeradius-init-db.sh /usr/local/bin/
COPY freeradius-db-test.sh /usr/local/bin/
COPY ds-init.sh /cbs/
RUN chmod a+x /usr/local/bin/freeradius*

CMD ["sh", "/cbs/ds-init.sh"]

