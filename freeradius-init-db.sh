#!/bin/bash
set -e

DEBIAN_FRONTEND=noninteractive

#wait for MySQL-Server to be ready
while ! mysqladmin ping -h"$MYSQL_HOST" --silent; do
    sleep 20
done

# Seed Database
mysql -h "$MYSQL_HOST" -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" < /etc/freeradius/3.0/mods-config/sql/main/mysql/schema.sql 
#mysql -h "$MYSQL_HOST" -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" < /etc/freeradius/3.0/mods-config/sql/main/mysql/nas.sql 
mysql -h "$MYSQL_HOST" -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" < /var/www/html/daloradius/contrib/db/mysql-daloradius.sql 
mysql -h "$MYSQL_HOST" -u root -p"$MYSQL_ROOT_PASSWORD" -e \
"GRANT ALL ON $MYSQL_DATABASE.* TO $MYSQL_USER@'%' IDENTIFIED BY '$MYSQL_PASSWORD'; \
flush privileges;"

echo '#!/bin/bash' > /usr/local/bin/freeradius-init-db.sh
echo 'exit 0' >> /usr/local/bin/freeradius-init-db.sh

exit 0
